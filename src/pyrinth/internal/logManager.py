import logging
from enum import Enum
from logging import Logger


class LogLevel(Enum):
	DEBUG = logging.DEBUG
	INFO = logging.INFO
	WARNING = logging.WARNING
	WARN = logging.WARN
	ERROR = logging.ERROR
	CRITICAL = logging.CRITICAL
	FATAL = logging.FATAL


class LogManager:
	_loggers: list[ Logger ]
	_level: LogLevel = LogLevel.INFO

	def __init__( self ) -> None:
		self._loggers = []

	def getLogger( self, name: str = 'PyRinth' ) -> Logger:
		""" Returns a logger registered as PyRinth logger """
		( logger := logging.getLogger( name ) ).setLevel( self._level.value )
		self._loggers.append( logger )
		return logger

	def setLevel( self, level: LogLevel ) -> None:
		""" Disables or enables the library's logging """
		self._level = level
		for logger in self._loggers:
			logger.setLevel( level.value )

	@classmethod
	def instance( cls ) -> 'LogManager':
		""" Returns the LogManager singleton """
		return _manager


_manager: LogManager = LogManager()
