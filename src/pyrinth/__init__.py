"""
PyRinth
-
A library to interact with modrinth's v1 and v2 APIs

Features:

 - Compatible with both v1 and v2 APIs
 - Automatic request caching and refreshing with configurable interval
 - Lazy population of objects to minimize API calls
 - Logging for API calls and stuff
"""
import requests

from pyrinth.exception import ProjectNotFoundException
from pyrinth.internal.logManager import LogManager, LogLevel
from pyrinth.internal.sessionManager import SessionManager

from pyrinth.model import v2
from pyrinth.model.v2.project import Project, LazyProject

__all__ = [
	'init',
	'LogManager',
	'LogLevel',
	'findProjectBy'
]
__version__ = '0.1.2'
__pdoc__ = { 'pyrinth.internal': False }


_logger = LogManager.instance().getLogger()


def init( authToken: str = None, logLevel: LogLevel = LogLevel.INFO ) -> None:
	"""
	Initializes the library, must be called before use

	:param authToken: GitHub auth token for modrinth auth
	:param logLevel: What level PyRinth should use when log stuff, may be used to hide PyRinth's logging messages
	"""
	_logger.info(f'Initializing PyRinth v{__version__}!')
	LogManager.instance().setLevel( logLevel )
	SessionManager.instance().setGithubToken( authToken )


def findProjectBy( id: str = None, name: str = None, lazy: bool = False ) -> Project:
	"""
	Finds a project on modrinth by its ID

	:param id: ID to search for
	:param name: name to search for
	:param lazy: Whether the returning object is cached and lazily populated
	:raises ProjectNotFound: if the project doesn't exist
	"""
	if lazy:
		return LazyProject.forId( id ) if id is not None else LazyProject.forName( name )
	res = v2.getRequest( f'project/{id or name}' )
	if res.ok:
		return Project.fromJson( res.json() )
	raise ProjectNotFoundException( id or name )
