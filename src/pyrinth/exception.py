class ProjectNotFoundException( Exception ):

	def __init__( self, projId: str ) -> None:
		super(ProjectNotFoundException, self).__init__( f'Couldn\'t find project {projId}' )
