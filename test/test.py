from unittest import TestCase

from pyrinth.model.license import Licenses
from pyrinth.model.v2.project import LazyProject, Project


class Test(TestCase):

	def testLazyProjects( self ) -> None:
		x = None

		with self.subTest( 'LazyProject instantiation' ):
			x = LazyProject.forId('4iX0pdTd')

		with self.subTest( 'LazyProject instance check' ):
			assert isinstance( x, LazyProject )

		with self.subTest( 'License is same MIT object ' ):
			assert x.license is Licenses.MIT

		with self.subTest( 'LazyProject impl is Project instance' ):
			assert isinstance( x._impl, Project )

		with self.subTest( 'Two same-id-LazyProjects are the same object' ):
			y = LazyProject.forId('4iX0pdTd')
			assert y.id == '4iX0pdTd'
			assert x._impl is y._impl


if __name__ == '__main__':
	Test().testProjects()
