import traceback

import pyrinth
from pyrinth import ProjectNotFoundException

pyrinth.init()


def main():
	print( f'PyRinth test project v0, type `help` for help.' )
	while True:
		cmd = input('>>> ').split()
		match cmd:
			case [ 'q', *_ ] | [ 'quit', *_ ]:
				print('Bye!')
				break
			case [ 'show', 'project', projId ]:
				print( f'Finding project with id "{projId}"..' )
				try:
					proj = pyrinth.findProjectBy( projId )
					print( f'name: {proj.title}' )
					print( f'team: {proj.team}' )
					print( f'status: {proj.status}' )
					if len( proj.versions ) > 0:
						print( f'latest release title: {proj.versions[0].name}' )
						print( f'latest release version: {proj.versions[0].version_number}' )
						print( f'latest release game version: {proj.versions[0].game_versions}' )
					else:
						print( f'latest release: N/D' )
				except ProjectNotFoundException:
					print( f'No projects found with that id' )


if __name__ == '__main__':
	main()
