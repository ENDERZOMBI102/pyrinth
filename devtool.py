import os
import sys
from argparse import ArgumentParser, Namespace
from types import FunctionType
from typing import Any

config: dict[str, Any] = {
	'package_name': 'pyrinth',
	'version': '1.0.0'
}


class Arguments:
	_PROG_ARGS: list['Arguments'] = []
	_SUBCOMMS: dict[ str, 'Arguments' ] = {}

	func: FunctionType
	name: str
	help: str

	@staticmethod
	def create( func: FunctionType ) -> FunctionType:
		arg = Arguments()
		arg.help = func.__doc__
		arg.name = func.__name__
		arg.func = func

		Arguments._PROG_ARGS.append( arg )
		return func

	@staticmethod
	def subcommand( func: FunctionType ) -> FunctionType:
		arg = Arguments()
		arg.help = func.__doc__
		arg.name = func.__name__
		arg.func = func

		Arguments._SUBCOMMS[arg.name] = arg
		return func

	@staticmethod
	def createParser() -> ArgumentParser:
		parser = ArgumentParser(
			prog='devtool.py',
			description='Various utilities for developers'
		)
		for argument in Arguments._PROG_ARGS:
			parser.add_argument(
				f'--{argument.name}',
				help=argument.help,
				action='store_true'
			)

		parser.add_argument(
			'action',
			action='store',
			choices=Arguments._SUBCOMMS.keys(),
			help='Action to perform'
		)

		parser.add_argument(
			'--version',
			action='version',
			version=config[ 'version' ]
		)

		return parser

	@staticmethod
	def run() -> None:
		args = Arguments.createParser().parse_args( sys.argv[1:] )
		Arguments._SUBCOMMS[ args.action ].func( args )


@Arguments.subcommand
def pdoc3( args: Namespace ):
	""" Runs pdoc3, generating the documentation """
	import pdoc.cli
	os.chdir('src')
	pdoc.cli.main( pdoc.cli.parser.parse_args( [
		'--html',
		config['package_name'],
		'-o',
		'../docs',
		'--force'
	] ) )


@Arguments.subcommand
def mypy( args: Namespace ):
	""" Runs pdoc3, generating the documentation """
	import mypy.__main__ as mypyc
	mypyc.main(
		script_path=None,
		stdout=sys.stdout,
		stderr=sys.stderr,
		args=[
			'src',
			'--color-output',
			'--pretty'
		]
	)


if __name__ == '__main__':
	Arguments.run()
