PyRinth
-
A library to interact with modrinth's v1 and v2 APIs

Features:
 - Compatible with both v1 and v2 APIs
 - Automatic request caching and refreshing with configurable interval
 - Lazy population of objects to minimize API calls

[Documentation](https://enderzombi102.gitlab.io/pyrinth)

### Installing
From PyPi
```bash
$ pip install pyrinth
```
From source
```bash
$ pip install git+https://gitlab.com/ENDERZOMBI102/pyrinth.git
```
